'use strict';

jQuery(document).ready(function(){
    jQuery(".menu__button-close").click(function () {
            $(this).css({'display' : 'none'});
            $('.menu__wrapper').css({'display' : 'block'})
            $('.menu__button-open').css({'display' : 'block'})
        });
    jQuery(".menu__button-open").click(function () {
        $(this).css({'display' : 'none'});
        $('.menu__wrapper').css({'display' : 'none'})
        $('.menu__button-close').css({'display' : 'block'})
    });

    $(window).resize(function(){
        let x = $(window).width();
        if(x > 768){
            $('.menu__wrapper').css({'display' : 'none'})
            $('.menu__button-open').css({'display' : 'none'})
            $('.menu__button-close').css({'display' : 'none'})
        }

        if(x <= 768){
            if($('.menu__button-open').css('display') == 'block'){
                $('.menu__button-close').css({'display' : 'none'})
            }else{
                $('.menu__button-close').css({'display' : 'block'})
            }
        }
    })
});

