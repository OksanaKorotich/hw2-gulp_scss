const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefix = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const clear = require('gulp-clean');
const browsersync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');

function browserServe(stop){
    browsersync.init({
        server: {
            baseDir: './DIST'
        }
    })
    stop()
}

function html(){
    return gulp.src('src/index.html')
    .pipe(gulp.dest('DIST'))
}

function css(){
    return gulp.src('src/scss/*.scss')
    .pipe(sass())
    .pipe(autoprefix({
        cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(rename({
        suffix: ".min",
    }))
    .pipe(gulp.dest('DIST'))
}

function js(){
    return gulp.src('./src/scripts/*.js')
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(rename({
        suffix: ".min",
    }))
    .pipe(gulp.dest('./DIST'))
}

function clearDist(){
    return gulp.src('./DIST', {allowEmpty: true})
    .pipe(clear())
}

function compress(){
    return gulp.src('./src/IMG/*.*')
    .pipe(imagemin())
    .pipe(gulp.dest('./DIST/img'))
}

function watch(){
    gulp.watch('./src/**/*.*', gulp.series(clearDist, gulp.parallel(html, css, js, compress))).on('all', browsersync.reload)
}

exports.html = html;
exports.css = css;
exports.js = js;
exports.clearDist = clearDist;
exports.watch = watch;

exports.build = gulp.series(clearDist, html, css, js, compress);
exports.dev = gulp.series(html, css, js, compress, browserServe, watch);
